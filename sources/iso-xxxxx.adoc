= Computer applications in terminology -- Naming -- Structure of personal names
:docnumber: XXXXX
:tc-document-number: 9999
:edition: 1
:ref-docnumber: ISO XXXXX:XXXX(E)
:copyright-year: 2019
:language: en
:title-intro-en: Computer applications in terminology
:title-main-en: Naming -- Structure of personal names
:title-intro-fr: Applications informatiques en terminologie
:title-main-fr: Nommage -- Structure des noms de personnes
:doctype: international-standard
:docstage: 10
:docsubstage: 92
:technical-committee-type: TC
:technical-committee-number: 37
:technical-committee: Language and terminology
:subcommittee-type: SC
:subcommittee-number: 4
:subcommittee: Language resource management
:workgroup-type: WG
:workgroup-number: 4
:workgroup: Lexical Resources
:secretariat: KATS
:language: en
:draft:
:toc:
:stem:
:xrefstyle: short
:imagesdir: images
:docfile: iso-xxxxx.adoc
:mn-document-class: iso
:mn-output-extensions: xml,html,doc,html_alt,rxl
:local-cache-only:
:data-uri-image:


include::sections-iso/00-foreword.adoc[]
include::sections/0b-intro.adoc[]

include::sections/01-scope.adoc[]

include::sections/02-norm-refs.adoc[]

include::sections/03-termsdef.adoc[]

include::sections/04-data-types.adoc[]

include::sections/05-personal-name.adoc[]

include::sections/06-name-component.adoc[]

include::sections/07-name-pattern.adoc[]

include::sections/08-name-profile.adoc[]

include::sections/a1-annex.adoc[]
include::sections/aa-honorifics.adoc[]
include::sections/ab-occupation.adoc[]

include::sections/a2-cultures.adoc[]
include::sections/zz-references.adoc[]
