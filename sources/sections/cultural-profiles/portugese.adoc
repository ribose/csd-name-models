
=== Portuguese

name components:

- given name
- matronymic name
- patronymic name

Portuguese often have two personal names, although the first may at times be used on its own.

==== Typical name patterns

|===
|given name|mother's patronymic name|father's patronymic name
|===

==== Examples

[example]
George Agostinho Baptista da Silva

[example]
"`António Caetano de Abreu Freire Egas Moniz`", Nobel prize winner. He was the
child of "`Fernando de Pina Rezende Abreu`" and "`Maria do Rosario de Almeida e
Sousa`".
